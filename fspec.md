# Function specification
____

Specifikace řídící jednotky budou vycházet částečně ze specifikace původní stavebnicové jednotky od Fischer technik a requirementů od zákazníka.

**Zákaznik vyžaduje:** 
 - Grafický displej
 - Tlačítka nebo enkodér
 - Výstup pro řízení motorku, včetně měření proudu
 - 4x digitální výstup se snímacím rezistorem pro měření proudu DAQ deskou od NI
 - 4x digitální vstup se svorkou pro připojení DAQ desky od NI
  
**Jednotka od Fischer Technik obsahuje:**
 - Grafický displej s kapacitním digitizérem
 - 8 universálních vstupů: Digital/Analog 0-9VDC, Analog 0-5 kΩ 
 - 4 vstupů na čítač, max 1 kHz
 - 8 výstupů
   - Možno řídit 4 motory, nebo 8 LED
   - PWM a short circuit ochrana
 - 3 PWM výstupu pro řízení modelářských serv

## Návrh

Je tendence navrhnout řídící jednotku ne pouze jednoúčelově ale využitelnou i s jinými stavebnicemi/roboty.
Hardware a realizace se může a bude volně inspirovat komerčním open-source řešením [Arduino Portenta machine control](https://www.arduino.cc/pro/portenta-machine-control).

 - **LCD display cca 3" s kapacitním dotykem**
   - Připojení sériovou sběrnící (SPI).
 - **Rotační enkodér s pushbuttonem.**
   - Přímé připojení na spražený timer v STM32 (Timer encoder mode)
 - min 8 vstupů analogových/digitálních (přepínatelné ze SW)
   - ESD ochrana (student proof design)
   - Navrhnout upravitelné zapojení s možností zapojení RC filtru a napěťového děliče
   - S největší pravděpodobností logická úroveň 0-10V.
   - Připravené pozice pro externí pull-up pull-down resistor
 - 8 výstupů 
 - 2 H můstky pro řízení DC motorků pomocí PWM.
   - Nutnost umět měřit proud.
     - Použití Proudového zesilovače TI řady INA s analogovým výstupem.
     - Ideálně jeden inline (přímo na výstupu můstku) a druhý highside sensor (Na napájení můstku).
       - Umožní detekci více chybových stavů, například zkratovaného H můstku nebo přerušeného napájení.
   - Výstup 10 V.
   - Nastavitelná frekvence PWM (Nastavení hodin timeru).
 - 4 vstupy pro čítač
   - *Pro použití napríklad s magnetickým enkodérem.*
 - **USB-C konektivita**
   - Virtual COM port
   - DFU bootloader
   - Komunikace s MATLAB
     - Nejdříve primitivně přes VCOM, později může být implementováno složitějších metod přenosu do MATLABu
 - ~~Konektivita s NI deskou~~
   - **Plná náhrada v podobě USB-C na desce a komunikace**