#ifndef __GPIOS_H__
#define __GPIOS_H__

#include <zephyr/drivers/gpio.h>
#include <zephyr/device.h>
#include <stdbool.h>
#include <stdint.h>

struct virtual_gpio {
    uint16_t value;
    uint16_t value_prev;
    bool malfunction_en;
};

int gpios_init(void);

extern volatile struct virtual_gpio virt_light_sw_1;
extern volatile struct virtual_gpio virt_light_sw_2;
extern volatile struct virtual_gpio virt_rev_limit_sw;

uint16_t virt_gpio_get_value(struct virtual_gpio gpio_pin, const uint16_t default_val);

void read_gpios(void);
void write_gpios(void);

#endif /*__GPIOS_H__*/