#ifndef __FSM_H__
#define __FSM_H__

#include <zephyr/smf.h>
#include <stdint.h>
#include <stdbool.h>

/* List of demo states */
enum BELT_FSM_STATES {S_STDBY, S_MOVING, S_WAITING};

struct s_object {
        /* This must be first */
        struct smf_ctx ctx;

        /* Other state specific data add here */
};

extern struct s_object s_obj;

extern const struct smf_state belt_fsm[];
extern uint8_t state_idx; 

int fsm_init(void);


#endif /*__FSM_H__*/