#include "fsm.h"
#include "gpios.h"
#include "pwm.h"

struct s_object s_obj;
uint8_t state_idx;

/* State S_STDBY (Initial one) */
void stdby_entry(void *o)
{
    virt_pwm_motor.pwm_en = false;
    state_idx = S_STDBY;
}

void stdby_run(void *o)
{
    /* state transition condition */
    if (virt_light_sw_1.value > virt_light_sw_1.value_prev)
        smf_set_state(SMF_CTX(&s_obj), &belt_fsm[S_MOVING]);
}


/* State S_MOVING */
void moving_entry(void *o)
{
    virt_pwm_motor.pwm_en = true;
    state_idx = S_MOVING;
}

void moving_run(void *o)
{
    /* state transition condition */
    if (virt_light_sw_2.value > virt_light_sw_2.value_prev)
        smf_set_state(SMF_CTX(&s_obj), &belt_fsm[S_WAITING]);
}


/* State S_WAITING */
void waiting_entry(void *o) 
{
    virt_pwm_motor.pwm_en = false;
    state_idx = S_WAITING;
}

void waiting_run(void *o)
{
    /* state transition condition */
    if (virt_light_sw_2.value < virt_light_sw_2.value_prev)
        smf_set_state(SMF_CTX(&s_obj), &belt_fsm[S_STDBY]);
}

/* Populate state table */
/* [STATE] = SMF_CREATE_STATE(STATE_ENTRY, STATE_LOOP, STATE_EXIT, NULL, NULL), */
const struct smf_state belt_fsm[] = {
        [S_STDBY]     = SMF_CREATE_STATE(stdby_entry,   stdby_run,     NULL),
        [S_MOVING]    = SMF_CREATE_STATE(moving_entry,  moving_run,    NULL),
        [S_WAITING]   = SMF_CREATE_STATE(waiting_entry, waiting_run,   NULL)
};


int fsm_init(void)
{
    /* Set initial state */
    smf_set_initial(SMF_CTX(&s_obj), &belt_fsm[S_STDBY]);

    state_idx = S_STDBY;

    return 0;
}