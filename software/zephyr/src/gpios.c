#include <zephyr/kernel.h>

#include "gpios.h"

// #include "uart.h"
// #include <string.h>

/* outputs */
static const struct gpio_dt_spec motor_dir       	= GPIO_DT_SPEC_GET(DT_NODELABEL(motor_dir), gpios);
static const struct gpio_dt_spec motor_gainsel   	= GPIO_DT_SPEC_GET(DT_NODELABEL(motor_gainsel), gpios);
static const struct gpio_dt_spec rev_limit_mirror	= GPIO_DT_SPEC_GET(DT_NODELABEL(rev_limit_mirror), gpios);
static const struct gpio_dt_spec light_sw_mirror_1	= GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(light_barriers_mirror), gpios, 0);
static const struct gpio_dt_spec light_sw_mirror_2	= GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(light_barriers_mirror), gpios, 1);

/* inputs */
static const struct gpio_dt_spec rev_limit_sw	= GPIO_DT_SPEC_GET(DT_NODELABEL(rev_limit_sw), gpios);
static const struct gpio_dt_spec light_sw_1  	= GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(light_barriers), gpios, 0);
static const struct gpio_dt_spec light_sw_2  	= GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(light_barriers), gpios, 1);

volatile struct virtual_gpio virt_light_sw_1;
volatile struct virtual_gpio virt_light_sw_2;
volatile struct virtual_gpio virt_rev_limit_sw;


int gpios_init(void)
{
	/* Init HW */
	if (!device_is_ready(motor_dir.port)) return -1;
	if (!device_is_ready(motor_gainsel.port)) return -1;
	if (!device_is_ready(rev_limit_sw.port)) return -1;
	if (!device_is_ready(light_sw_1.port)) return -1;
	if (!device_is_ready(light_sw_2.port)) return -1;


	/* Set default values */
	gpio_pin_configure_dt(&motor_dir, GPIO_OUTPUT_INACTIVE);
	gpio_pin_configure_dt(&motor_gainsel, GPIO_DISCONNECTED);
	// gpio_pin_configure_dt(&motor_gainsel, GPIO_OUTPUT_ACTIVE);
	gpio_pin_configure_dt(&rev_limit_mirror, GPIO_OUTPUT_INACTIVE);
	gpio_pin_configure_dt(&light_sw_mirror_1, GPIO_OUTPUT_INACTIVE);
	gpio_pin_configure_dt(&light_sw_mirror_2, GPIO_OUTPUT_INACTIVE);


	gpio_pin_configure_dt(&rev_limit_sw, GPIO_INPUT);
	gpio_pin_configure_dt(&light_sw_1, GPIO_INPUT);
	gpio_pin_configure_dt(&light_sw_2, GPIO_INPUT);


	/* init virtual values */
	virt_light_sw_1.value = 0;
	virt_light_sw_1.value_prev = 0;
	virt_light_sw_1.malfunction_en = false;

	virt_light_sw_2.value = 0;
	virt_light_sw_2.value_prev = 0;
	virt_light_sw_2.malfunction_en = false;

	virt_rev_limit_sw.value = 0;
	virt_rev_limit_sw.value_prev = 0;
	virt_rev_limit_sw.malfunction_en = false;

    return 0;
}

uint16_t virt_gpio_get_value(struct virtual_gpio gpio_pin, const uint16_t default_val)
{
    uint16_t ret = gpio_pin.value;

    if (gpio_pin.malfunction_en) ret = default_val;

    return ret;
}

void read_gpios(void)
{
	virt_light_sw_1.value_prev  	= virt_light_sw_1.value;
	virt_light_sw_2.value_prev  	= virt_light_sw_2.value;
	virt_rev_limit_sw.value_prev	= virt_rev_limit_sw.value;

	virt_light_sw_1.value  	= gpio_pin_get_dt(&light_sw_1);
	virt_light_sw_2.value  	= gpio_pin_get_dt(&light_sw_2);
	virt_rev_limit_sw.value	= gpio_pin_get_dt(&rev_limit_sw);

	virt_light_sw_1.value  	= virt_gpio_get_value(virt_light_sw_1, 0);
	virt_light_sw_2.value  	= virt_gpio_get_value(virt_light_sw_2, 0);
	virt_rev_limit_sw.value	= virt_gpio_get_value(virt_rev_limit_sw, 0);
}

void write_gpios(void)
{
	gpio_pin_set_dt(&rev_limit_mirror, virt_gpio_get_value(virt_rev_limit_sw, 0));
	gpio_pin_set_dt(&light_sw_mirror_1, virt_gpio_get_value(virt_light_sw_1, 0));
	gpio_pin_set_dt(&light_sw_mirror_2, virt_gpio_get_value(virt_light_sw_2, 0));
}