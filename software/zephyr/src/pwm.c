#include <zephyr/kernel.h>

#include "pwm.h"

#define PWM_OFF (0)
#define PWM_ON (PWM_USEC(75))
#define PWM_SLOW (PWM_USEC(25))

static const struct pwm_dt_spec pwm_motor = PWM_DT_SPEC_GET(DT_NODELABEL(pwm_motor));

volatile struct virt_pwm virt_pwm_motor;

int pwm_init(void)
{
	if (!pwm_is_ready_dt(&pwm_motor)) return -1;

    
	pwm_set_pulse_dt(&pwm_motor, PWM_OFF);

	virt_pwm_motor.pwm_en        	= false;
	virt_pwm_motor.slow_down_en  	= false;
	virt_pwm_motor.malfunction_en	= false;

    return 0;
}


/* Yet this is single purpose function, in future it can generalised */
void update_virt_pwm_state(struct virt_pwm pin)
{
	uint32_t pulse_len;

	if (pin.pwm_en) {
		pulse_len = PWM_ON;
		if (pin.malfunction_en) {
			pulse_len = PWM_OFF;
		} else {
			if (pin.slow_down_en) pulse_len = PWM_SLOW;
		}
	} else {
		if (pin.malfunction_en) {
			pulse_len = PWM_ON;
		} else {
			pulse_len = PWM_OFF;
		}
	} 

	pwm_set_pulse_dt(&pwm_motor, pulse_len);
}