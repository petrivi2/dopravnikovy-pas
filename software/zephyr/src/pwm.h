#ifndef __PWM_H__
#define __PWM_H__

#include <zephyr/drivers/pwm.h>
#include <zephyr/device.h>
#include <stdint.h>
#include <stdbool.h>

struct virt_pwm {
    bool pwm_en;
    bool slow_down_en;
    bool malfunction_en;
};

int pwm_init(void);

extern volatile struct virt_pwm virt_pwm_motor;

void update_virt_pwm_state(struct virt_pwm pin);


#endif /*__PWM_H__*/