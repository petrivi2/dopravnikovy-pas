#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/uart.h>

#include <string.h>
#include "uart.h"
#include "gpios.h"
#include "pwm.h"

/* change this to any other UART peripheral if desired */
// UART_2 115200 8N1
// #define UART_DEVICE_NODE DT_CHOSEN(zephyr_shell_uart)


#define SW_1_ERROR_CODE      	(0x00U)
#define SW_2_ERROR_CODE      	(0x01U)
#define REV_SW_ERROR_CODE    	(0x02U)
#define MOTOR_SLOW_ERROR_CODE	(0x03U)
#define MOTOR_OFF_ERROR_CODE 	(0x04U)

static const struct device *const uart_dev = DEVICE_DT_GET(DT_NODELABEL(usart1));


void serial_cb(const struct device *dev, void *user_data)
{

	if (!uart_irq_update(uart_dev)) return;

	if (!uart_irq_rx_ready(uart_dev)) return;

	/* read until FIFO empty */
	uint8_t received_byte;
	if (uart_fifo_read(uart_dev, &received_byte, 1) == 1) {
		const uint8_t malfunction_code = received_byte>>4;
		const uint8_t value = received_byte & 0x01;

		printk("Code: %d Val: %d\r\n", malfunction_code, value);

		switch (malfunction_code)
		{

		case (SW_1_ERROR_CODE):
			if (value) {
		 		virt_light_sw_1.malfunction_en = true;
			} else {
		 		virt_light_sw_1.malfunction_en = false;
			}
			printk("SW_1: %d\r\n", virt_light_sw_1.malfunction_en);
			break;

		case (SW_2_ERROR_CODE):
			if (value) {
		 		virt_light_sw_2.malfunction_en = true;
			} else {
		 		virt_light_sw_2.malfunction_en = false;
			}
			printk("SW_2: %d\r\n", virt_light_sw_1.malfunction_en);
			break;

		case (REV_SW_ERROR_CODE):
			if (value) {
		 		virt_rev_limit_sw.malfunction_en = true;
			} else {
		 		virt_rev_limit_sw.malfunction_en = false;
			}
			printk("Rev. Limit Switch: %d\r\n", virt_light_sw_1.malfunction_en);
			break;

		case (MOTOR_SLOW_ERROR_CODE):
			if (value) {
		 		virt_pwm_motor.slow_down_en = true;
			} else {
		 		virt_pwm_motor.slow_down_en = false;
			}
			printk("Motor Slow Down: %d\r\n", virt_light_sw_1.malfunction_en);
			break;

		case (MOTOR_OFF_ERROR_CODE):
			if (value) {
		 		virt_pwm_motor.malfunction_en = true;
			} else {
		 		virt_pwm_motor.malfunction_en = false;
			}
			printk("Motor Off: %d\r\n", virt_light_sw_1.malfunction_en);
			break;

		default:
			break;
		}

	}
}

void print_uart(uint8_t *buf, const int nmemb)
{
	for (int i = 0; i < nmemb; i++) 
        uart_poll_out(uart_dev, buf[i]);
}

int uart_init(void)
{
	if (!device_is_ready(uart_dev)) return -1;

	/* configure interrupt and callback to receive data */
	int ret = uart_irq_callback_user_data_set(uart_dev, serial_cb, NULL);

	if (ret < 0) return ret;

	uart_irq_rx_enable(uart_dev);


	return 0;
}
