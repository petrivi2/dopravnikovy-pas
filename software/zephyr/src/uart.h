#ifndef __UART_H__
#define __UART_H__

#include <stdint.h>

#define DEFAULT_UART_BUF_SIZE (32)

void print_uart(uint8_t *buf, const int nmemb);

int uart_init(void);

#endif /*__UART_H__*/