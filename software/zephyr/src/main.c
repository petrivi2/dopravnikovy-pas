#include <zephyr/kernel.h>

#include <stdint.h>
#include <string.h>

#include "uart.h"
#include "gpios.h"
#include "pwm.h"
#include "fsm.h"

#define THREAD_STACK_SIZE (1024)

K_THREAD_STACK_DEFINE(th_display_updates_stack, THREAD_STACK_SIZE);
#define TH_DISPLAY_UPDATES_PRIORITY (50)
struct k_thread th_display_updates_data;

// K_THREAD_STACK_DEFINE(th_vcom_stack, THREAD_STACK_SIZE);
// struct k_thread th_vcom_data;

#define MAIN_LOOP_PERIOD         	(K_MSEC(200))
#define DISPLAY_UPDATE_LOOP_PERIOD	(K_MSEC(100))

void th_display_updates(void *p1, void *p2, void *p3)
{
	char t0_buf[DEFAULT_UART_BUF_SIZE];
	char t1_buf[DEFAULT_UART_BUF_SIZE];
	char t2_buf[DEFAULT_UART_BUF_SIZE];
	const uint16_t color_idle  	= 821;
	const uint16_t color_active	= 62784;
	const uint8_t validity_seq_size = 3;
	uint8_t validity_seq[] = {0xFF,0xFF,0xFF};

	for (;;) {
		switch (state_idx)
		{

		case S_STDBY:
			sprintf(t0_buf, "t0.bco=%d", color_active);
			sprintf(t1_buf, "t1.bco=%d", color_idle);
			sprintf(t2_buf, "t2.bco=%d", color_idle);
			break;

		case S_MOVING:
			sprintf(t0_buf, "t0.bco=%d", color_idle);
			sprintf(t1_buf, "t1.bco=%d", color_active);
			sprintf(t2_buf, "t2.bco=%d", color_idle);
			break;

		case S_WAITING:
			sprintf(t0_buf, "t0.bco=%d", color_idle);
			sprintf(t1_buf, "t1.bco=%d", color_idle);
			sprintf(t2_buf, "t2.bco=%d", color_active);
			break;

		default:
			break;
		}
		print_uart(t0_buf, strlen(t0_buf));
		print_uart(validity_seq, validity_seq_size);

		print_uart(t1_buf, strlen(t1_buf));
		print_uart(validity_seq, validity_seq_size);

		print_uart(t2_buf, strlen(t2_buf));
		print_uart(validity_seq, validity_seq_size);
		}

		k_sleep(DISPLAY_UPDATE_LOOP_PERIOD);
}

int main(void)
{
	if (gpios_init()) {
		printk("GPIOs initialization ..... [FAIL]\n");
		return -1;
	}

	printk("GPIOs initialization ..... [OK]\n");

	if (pwm_init()) {
		printk("PWM signals initialization ..... [FAIL]\n");
		return -1;
	}

	printk("PWM signals initialization ..... [OK]\n");


	if (uart_init()) {
		printk("UART initialization ..... [FAIL]\n");
		return -1;
	}

	printk("UART initialization ..... [OK]\n");

	if (fsm_init()) {
		printk("FSM initialization ..... [FAIL]\n");
		return -1;
	}

	printk("FSM initialization ..... [OK]\n");


	printk("Threads started ..... [OK]\n");

	k_tid_t th_display_updates_tid = k_thread_create(&th_display_updates_data, th_display_updates_stack, K_THREAD_STACK_SIZEOF(th_display_updates_stack), 
	                                th_display_updates, NULL, NULL, NULL, TH_DISPLAY_UPDATES_PRIORITY, 0, K_NO_WAIT);

	printk("System is ready!\n");

	for (;;) {
		
		/* update inputs */
		read_gpios();

  		/* process inputs */
		smf_run_state(SMF_CTX(&s_obj));

		/* update outputs */
		update_virt_pwm_state(virt_pwm_motor);
		write_gpios();

		k_sleep(MAIN_LOOP_PERIOD);
	}

	return 0;
}
